const mongodb = require('mongodb')
const common = require('./common')

const db = module.exports = {
    connection: null,
    collation: 'en',
    init: (nextTick) => {
        mongodb.MongoClient.connect(common.config.dbUrl, { useUnifiedTopology: true }, (err, connection) => {
            if(err) {
                console.error(err.message)
                process.exit(0)
            }
            db.connection = connection.db(common.config.dbName)
            db.users = db.connection.collection('users')
            db.users.count((err, n) => {
                if(n < 1) {
                    console.log('Create default admin account', config.defaultAdmin)
                    db.users.insertOne({ username: common.config.defaultAdmin, password: common.config.defaultAdminPassword, roles: [ 0 ] })        
                }
            })
            console.log('Connected to database', common.config.dbUrl + '/' + common.config.dbName)
            if(common.config.collation) db.collation = common.config.collation
            nextTick()
        }) 
    },
    ObjectId: mongodb.ObjectId,
    users: null
}