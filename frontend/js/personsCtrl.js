app.controller('PersonsCtrl', [ '$http', 'common', function($http, common) {
    let ctrl = this

    ctrl.edited = -1
    ctrl.persons = []
    ctrl.person = {}
    ctrl.search = ''
    ctrl.limit = 10

    const endpoint = '/user/persons'
    
    const clearPerson = function() {
        ctrl.person = {
            firstName: '',
            lastName: '',
            yearOfBirth: 1900,
            email: ''
        }
    }

    const copyPerson = function(person) {
        return {
            firstName: person.firstName,
            lastName: person.lastName,
            yearOfBirth: person.yearOfBirth,
            email: person.email
        }
    }

    let ajv = null
    let schema = null
    let schemaValidator = function() { return true }
    
    ctrl.validate = function() {
        return schemaValidator(ctrl.person)
    }

    ctrl.post = function() {
        $http.post(endpoint, ctrl.person).then(
            function(res) {
                ctrl.get()
                clearPerson() 
            },
            function(err) {
                common.alert(err.data.error, 'danger')
            }
        )           
    }

    ctrl.delete = function(index) {
        $http.delete(endpoint + '?_id=' + ctrl.persons[index]._id).then(
            function(res) {
                ctrl.get()
            },
            function(err) {}
        )   
    }

    ctrl.get = function() {
        $http.get(endpoint + '?search=' + ctrl.search + '&limit=' + ctrl.limit).then(
            function(res) { 
                ctrl.persons = res.data
            },
            function(err) {
                common.alert(err.data.error, 'danger')
            }
        )   
    }

    ctrl.edit = function(index) {
        ctrl.edited = index
        ctrl.person = copyPerson(ctrl.persons[index])
    }

    ctrl.cancel = function() {
        clearPerson()
        ctrl.edited = -1
    }

    ctrl.put = function() {
        $http.put(endpoint + '?_id=' + ctrl.persons[ctrl.edited]._id, ctrl.person).then(
            function(res) {
                clearPerson()
                ctrl.get()
                ctrl.edited = -1
            },
            function(err) {
                common.alert(err.data.error, 'danger')
            }
        )   
    }

    clearPerson()
    ctrl.get()

    $http.get(endpoint + '/schema').then(
        function(res) {
            schema = res.data
            ajv = new Ajv()
            schemaValidator = ajv.compile(schema)
        },
        function(err) {
            common.alert(err.data.error, 'danger')
        }
    )
}])