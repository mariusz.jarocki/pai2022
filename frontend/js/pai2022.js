const app = angular.module('pai2022', [ 'ngRoute', 'ngSanitize', 'ngAnimate', 'ui.bootstrap', 'ct-ui.select', 'httpLoadingInterceptor', 'cogAlert', 'ws' ])

// router menu
app.constant('routes', [
	{ route: '/', templateUrl: 'homeView.html', controller: 'HomeCtrl', controllerAs: 'ctrl', menu: '<i class="fa fa-lg fa-home"></i>', roles: null },
	{ route: '/admin', templateUrl: 'adminView.html', controller: 'AdminCtrl', controllerAs: 'ctrl', menu: 'Administracja', roles: [ 0 ] },
	{ route: '/persons', templateUrl: 'personsView.html', controller: 'PersonsCtrl', controllerAs: 'ctrl', menu: 'Osoby', roles: [ 1 ] },
	{ route: '/projects', templateUrl: 'projectsView.html', controller: 'ProjectsCtrl', controllerAs: 'ctrl', menu: 'Projekty', roles: [ 1 ] }
])

// router installation
app.config(['$routeProvider', '$locationProvider', 'routes', function($routeProvider, $locationProvider, routes) {
    $locationProvider.hashPrefix('')
	for(var i in routes) {
		$routeProvider.when(routes[i].route, routes[i])
	}
	$routeProvider.otherwise({ redirectTo: '/' })
}])

// websocket config
app.config(['wsProvider', function(wsProvider) {
    wsProvider.setUrl('ws://' + window.location.host)
}])

app.service('common', [ '$uibModal', 'Alerting', function($uibModal, Alerting) {
    let common = this

    common.env = {}

    common.alert = (text, type = 'info') => {
        Alerting.addAlert(type, text)
        console.log('alert-' + type, ':', text)
    }

    // general modal dialog
    common.dialog = function(templateUrl, controllerName, options, nextTick) {

        let modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title-top',
            ariaDescribedBy: 'modal-body-top',
            templateUrl: templateUrl,
            controller: controllerName,
            controllerAs: 'ctrl',
            resolve: {
                options: function () {
                    return options
                }
            }
        })

        modalInstance.result.then(
            function(answer) { nextTick(answer) },
            function() { nextTick(null) }
        )
    }    
}])

app.controller('MainCtrl', [ '$location', '$scope', '$rootScope', '$http', 'common', 'routes', 'ws', function($location, $scope, $rootScope, $http, common, routes, ws) {
	let ctrl = this

    ctrl.alert = common.alert

    ctrl.env = common.env

    ctrl.creds = {
        username: '',
        password: ''
    }

    const wsInit = function() {
        ws.send(JSON.stringify({ type: 'init', sessionid: ctrl.env.loggedUser.sessionid }))        
    }

    ctrl.doLogin = function() {
        $http.post('/auth', ctrl.creds).then(
            function(res) {
                ctrl.env.loggedUser = res.data
                rebuildMenu()
                common.alert('Witaj, ' + ctrl.env.loggedUser.username, 'success')
            },
            function(err) {
                common.alert('Logowanie nieudane', 'danger')
            }
        )
    }

    ctrl.doLogout = function() {
        let oldUser = ctrl.env.loggedUser ? ctrl.env.loggedUser.username : null
        $http.delete('/auth').then(
            function(res) {
                ctrl.env.loggedUser = res.data
                rebuildMenu()
                if(oldUser) {
                    common.alert('Żegnaj, ' + oldUser, 'success')
                }
            },
            function(err) {}
        )
    }

	// budowanie menu
    ctrl.menu = []

    const rebuildMenu = function() {
        ctrl.menu.length = 0
		for(let route of routes) {
            let intersection = []
            if(route.roles == null) {
                intersection.push(-1)
            } else {
                route.roles.forEach((roleNum) => {
                    if(ctrl.env.loggedUser.roles && ctrl.env.loggedUser.roles.includes(roleNum)) {
                        intersection.push(roleNum)
                    }
                })
            }
            if(intersection.length > 0) {
                ctrl.menu.push({ route: route.route, title: route.menu })
            }
		}
        $location.path('/')
        $rootScope.$broadcast('refresh')
        wsInit()
    }

    // kontrola nad menu zwiniętym i rozwiniętym
    ctrl.isCollapsed = true
    $scope.$on('$routeChangeSuccess', function () {
        ctrl.isCollapsed = true
    })
    
    // sprawdzenie która pozycja menu jest wybrana
    ctrl.navClass = function(page) {
        return page === $location.path() ? 'active' : ''
    }     

    ws.on('message', function(messageEvent) {
        console.log('Message from backend', messageEvent.data)
        try {
            let message = JSON.parse(messageEvent.data)
            common.alert('Message ' + JSON.stringify(message))
            if(message.type == 'refresh') {
                $rootScope.$broadcast('refresh', message.collection)
            }
        } catch(ex) {
            common.alert('Data from backend is not JSON', 'danger')
        }
    })    

    $http.get('/auth').then(
        function(res) {
            ctrl.env.loggedUser = res.data
            rebuildMenu()
        },
        function(err) {
            common.alert('Cannot get information about the session', 'danger')
        }
    )
}])