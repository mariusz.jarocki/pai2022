app.controller('AdminCtrl', [ 'common', function(common) {
    let ctrl = this

    let options = { title: 'Przykładowe dane', data: {}, ok: true, cancel: true }

    ctrl.modal = function() {
        common.dialog('adminDialog.html', 'AdminDialog', options, (result) => {
            switch(result) {
                case 'ok':
                    console.log(options.data)
                    break
            }           
        })
    }
}])