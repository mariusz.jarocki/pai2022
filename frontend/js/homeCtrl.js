app.controller('HomeCtrl', [ '$scope', '$http', 'ws', 'common', function($scope, $http, ws, common) {
    let ctrl = this
    
    ctrl.env = common.env

    ctrl.text = ''

    ctrl.send = function() {
        ws.send(JSON.stringify({ type: 'message', text: ctrl.text }))
    }

    ctrl.date = null
    ctrl.logDate = function() {
        console.log(new Date(ctrl.date).getTime())
    }

    ctrl.stats = {}

    const refresh = function() {
        if(ctrl.env.loggedUser.username) {
            $http.get('/stats').then(
                function(res) {
                    ctrl.stats = res.data
                },
                function(err) {}
            )
        }
    }

    refresh()

    $scope.$on('refresh', (event, collectionName) => {
        refresh()     
    })
}])