app.controller('ProjectsCtrl', [ '$http', 'common', function($http, common) {
    let ctrl = this

    ctrl.edited = -1
    ctrl.projects = []
    ctrl.project = {}

    ctrl.persons = []

    const endpoint = '/user/projects'
    
    const clearproject = function() {
        delete ctrl.project._id
        Object.assign(ctrl.project, {
            name: '',
            manager: null,
            members: []
        })    
    }

    ctrl.validate = function() {
        return ctrl.project.name
    }

    const clearMembers = (members) => {
        return members.map((member) => member._id)
    }

    ctrl.post = function() {
        ctrl.project.members = clearMembers(ctrl.project.members)
        $http.post(endpoint, ctrl.project).then(
            function(res) {
                get()
                clearproject() 
            },
            function(err) {
                common.alert(err.data.error, 'danger')
            }
        )   
    }

    ctrl.delete = function(index) {
        $http.delete(endpoint + '?_id=' + ctrl.projects[index]._id).then(
            function(res) {
                get()
            },
            function(err) {
                common.alert(err.data.error, 'danger')
            }
        )   
    }

    const get = function() {
        $http.get(endpoint).then(
            function(res) { 
                ctrl.projects = res.data
            },
            function(err) {
                common.alert(err.data.error, 'danger')
            }
        )   
    }

    ctrl.edit = function(index) {
        ctrl.edited = index
        Object.assign(ctrl.project, ctrl.projects[index])
        ctrl.project.manager = ctrl.projects[index].manager._id
    }

    ctrl.put = function() {
        ctrl.project.members = clearMembers(ctrl.project.members)
        $http.put(endpoint + '?_id=' + ctrl.projects[ctrl.edited]._id, ctrl.project).then(
            function(res) {
                clearproject()
                get()
                ctrl.edited = -1
            },
            function(err) {
                common.alert(err.data.error, 'danger')
            }
        )   
    }

    ctrl.cancel = function() {
        clearproject()
        ctrl.edited=-1
    }

    clearproject()
    get()

    $http.get('/user/persons').then(
        function(res) {
            ctrl.persons = res.data
        },
        function(err) {
            common.alert(err.data.error, 'danger')
        }
    )
}])