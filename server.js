const fs = require('fs')

const express = require('express')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const expressSession = require('express-session')
const passport = require('passport')
const passportJson = require('passport-json')
const expressWs = require('express-ws')

const common = require('./common')
const auth = require('./auth')
const collectionRest = require('./collectionRest')
const collectionConfig = require('./collectionConfig')
const db = require('./db')
const stats = require('./stats')

// config
try {
    common.config = JSON.parse(fs.readFileSync('config.json'))
} catch(ex) {
    console.error('Error reading configuration:', ex.message)
    process.exit(0)
}

// express initialization
const app = express()
common.wsInstance = expressWs(app)

app.use(morgan('tiny'))

// body parser
app.use(bodyParser.json())

// handle errors on body parser
app.use((err, req, res, nextTick) => {
    if(err) {
        res.status(400).json({ error: 'Invalid payload' })
    } else {
        console.log(req.method, req.url)
        nextTick()
    }
})

// authorization middleware
app.use(expressSession({ secret: common.config.appName, resave: false , saveUninitialized: true }))
app.use(passport.initialize())
app.use(passport.session())
passport.use(new passportJson.Strategy(auth.checkCredentials))
passport.serializeUser(auth.serialize)
passport.deserializeUser(auth.deserialize)

// authentication endpoints
app.get('/auth', auth.whoami)
app.post('/auth', passport.authenticate('json', { failWithError: true }), auth.login, auth.errorHandler)
app.delete('/auth', auth.logout)

// source of static content
app.use(express.static('frontend'))

// rest endpoints
app.all('/admin/:collection', auth.checkIfInRole([ 0 ]), collectionRest([ 'users' ]))
app.get('/sessions', auth.checkIfInRole([ 0 ]), (req, res) => {
    req.sessionStore.all((err, sessions) => {
        if(!err) {
            let transformedSessions = {}
            Object.keys(sessions).forEach((sessionid) => {
                transformedSessions[sessionid] = {
                    username: sessions[sessionid].passport ? sessions[sessionid].passport.user : null,
                    roles: sessions[sessionid].roles ? sessions[sessionid].roles : []
                }
            })
            res.json(transformedSessions)
        } else {
            res.status(400).json({ error: 'Sessions not available' })
        }
    })
})

app.all('/user/:collection', auth.checkIfInRole([ 1 ]), collectionRest([ 'persons', 'projects', 'tasks' ]))
app.get('/user/:collection/schema', auth.checkIfInRole([ 1 ]), collectionConfig.schema)

app.get('/stats', auth.checkIfInRole([ 0, 1 ]), stats)

// websockets handling
app.ws('/', (ws, req) => {
    ws.on('message', (message) => {
        try {
            console.log('WEBSOCKET raw data arrived', message)
            message = JSON.parse(message)
        } catch(ex) {
            console.error('WEBSOCKET error: non-json message')
            return
        }
        switch(message.type) {
            case 'init':
                ws.sessionid = message.sessionid || null
                console.log('WEBSOCKET init from session', ws.sessionid)
                break
            case 'message':
                console.log('WEBSOCKET message from', ws.sessionid)
                if(!ws.sessionid) {
                    console.error('WEBSOCKET sender unknown')
                    return
                }
                req.sessionStore.all((err, sessions) => {
                    if(!err) {
                        let from = sessions[ws.sessionid] && sessions[ws.sessionid].passport ? sessions[ws.sessionid].passport.user : null
                        let broadcast = JSON.stringify({ type: 'broadcast', from, text: message.text })
                        common.wsInstance.getWss().clients.forEach((client) => {
                            if(client != ws && client.readyState == client.OPEN) {
                                let to = sessions[client.sessionid] && sessions[client.sessionid].passport ? sessions[client.sessionid].passport.user : null
                                if(to) {
                                    console.log('WEBSOCKET sending', broadcast, 'to', to)
                                    client.send(broadcast)
                                }
                            }
                        })   
                    }
                })
                break 
            default:
                console.error('WEBSOCKET error: unknown message type', message.type)
        }
    })
})

// main process
console.log('Backend of', common.config.appName, 'is starting')
db.init(() => {
        try {
            app.listen(common.config.port)
            console.log('Listening on port', common.config.port)
        } catch(err) {
            console.error(err.message)
            process.exit(0)
        }
    }
)