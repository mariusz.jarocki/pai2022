const ajv = require('ajv')
const ajvFormats = require('ajv-formats')

const db = require('./db')

let Ajv = null

const validateBody = (config, body) => {
    if(!Ajv) {
        Ajv = new ajv()
        ajvFormats(Ajv)
    }
    if(!config.validate) config.validate = Ajv.compile(config.schema)
    if(config.validate(body)) return null
    let msgs = []
    for(let err of config.validate.errors) {
        msgs.push(err.message + err.instancePath)
    } 
    return { error: msgs.join() }
}

const collectionConfig = module.exports = {
    schema: (req, res) => {
        res.json(collectionConfig[req.params.collection].schema)
    }, 
    persons: {
        schema: {
            type: 'object',
            properties: {
                firstName: { type: 'string' },
                lastName: { type: 'string' },
                yearOfBirth: { type: 'integer' },
                email: { type: 'string', format: 'email' }
            },
            required: [ 'firstName', 'lastName', 'yearOfBirth' ],
            additionalProperties: false
        },
        filter: (search) => {
            return { $match: { $or: [
                { firstName: { $regex: '.*' + search + '.*', $options: 'i' }},
                { lastName: { $regex: '.*' + search + '.*', $options: 'i' }},
            ]}}
        },
        getAggregate: [
            { $sort: { lastName: 1, firstName: 1 } }
        ],
        prepareData: (body) => {
            let err = validateBody(collectionConfig.persons, body)
            if(err) return err
            return null
        }
    },
    projects: {
        getAggregate: [
            { $lookup: { from: 'persons', localField: 'manager', foreignField: '_id', as: 'manager' }},
            { $unwind: { path: '$manager', preserveNullAndEmptyArrays: true }},
            { $lookup: { from: 'persons', localField: 'members', foreignField: '_id', as: 'members' }},
            { $sort: { name: 1 }}
        ],
        prepareData: (body) => {
            if(body.manager) {
                try {
                    body.manager = db.ObjectId(body.manager)
                } catch {
                    body.manager = null
                }
            } else {
                body.manager = null
            }
            if(body.members && body.members.length > 0) {
                try {
                    body.members.forEach((member, index, arr) => { arr[index] = db.ObjectId(member) })
                } catch {
                    body.members = []
                }
            } else {
                body.members = []
            }
        }
    },
    tasks: {
        getAggregate: [
            { $lookup: { from: 'projects', localField: 'project', foreignField: '_id', as: 'project' }},
            { $unwind: { path: '$project', preserveNullAndEmptyArrays: true }},
            { $lookup: { from: 'persons', localField: 'executive', foreignField: '_id', as: 'executive' }},
            { $unwind: { path: '$executive', preserveNullAndEmptyArrays: true }},
            { $sort: { name: 1 }}
        ],
        prepareData: (body) => {
            if(body.project) {
                try {
                    body.project = db.ObjectId(body.project)
                } catch {
                    body.project = null
                }
            }
            if(body.executive) {
                try {
                    body.executive = db.ObjectId(body.executive)
                } catch {
                    body.executive = null
                }
            }
        },
        filter: (search) => {
            console.log(search)
            try {
                search = JSON.parse(search)
            } catch(ex) { return null }
            let project = null
            try {
                project = db.ObjectId(search.project)
            } catch(ex) {}
            let match = {}
            if(project) match['project._id'] = project
            if(search.name) match.name = { $regex: '.*' + search.name + '.*', $options: 'i' }
            return { $match: match }
        }
    }
}