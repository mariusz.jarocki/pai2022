# pai2022

## Jak zacząć

#### Instalacja

* git clone git@gitlab.com:mariusz.jarocki/pai2022.git
* cd pai2022
* npm install
* cd frontend
* npm install
* cd ..
* _copy_ config.json.example config.json
* _customize config.json_

#### Uruchomienie serwera

* npm start

#### URL

* http://localhost:5555

## Zaliczenie

#### Zadanie na 3

Obok osób i projektów w bazie danych są przechowywane informacje o zadaniach.
```json
{
  "_id": {
    "$oid": "63b2c2f1b04a76c6366a40a6"
  },
  "project": {
    "$oid": "6384f926007df353763e70dd"
  },
  "name": "Zadanie początkowe",
  "start_date": 1672659989,
  "end_date": 1673020800,
  "executives": {
    "$oid": "638c9217b87841b5d1a404a1"
  },
  "status": 0
}
```
Zadania są obsługiwane przez nową zakładkę nawigacyjną, dostępną z poziomu użytkowników w roli 1. Widok tej zakładki powinien umożliwiać:
* wybór projektu, którego zadaniami chcemy zarządzać
* dodawanie nowego zadania, wraz z domyślnymi wartościami pól start_date (dzisiaj) i end_date (jutro), wykorzystać należy input type="date"; wybór osób realizujących (executives) spośród członków (members) projektu, wybór ten realizowany jest przez checkboxy przy każdym członku projektu
* pole status może być zmieniane na inne wartości numeryczne przez combobox z nazwami opisowymi (0 - niezaczęte, 1 - w toku, 2 - w testach, 3 - gotowe, 4 - anulowane)

#### Zadanie na 4

Zadanie na 3 oraz dodatkowe funkcjonalności:
1. Do systemu może zalogować się nie tylko administrator (rola 0 - ten kto zarządza users) czy user (rola 1 - ten kto zarządza persons, projects i tasks), ale również każdy z kolekcji persons; aby było to możliwe, możemy potraktować email osoby jako login oraz wyposażyć obiekty kolekcji persons w dodatkowe pole password; przyjmijmy że jedynie osoby o zdefiniowanym password mogą zalogować się do systemu.
2. Rola takiego użytkownika pozwala mu na dostęp read-only do kolekcji persons, projects i tasks oraz dodatkowo read-write do kolekcji messages.
3. Rola ta ma dostęp do widoków projektów oraz zadań ale WYŁĄCZNIE do tych, które użytkownika dotyczą (użytkownik jest managerem lub członkiem zespołu projektowego)
4. Użytkownik ma nową zakładkę Wiadomości, która działa podobnie do klienta poczty elektronicznej tj. pozwala na odczytanie wszystkich wiadomości, które zostały wysłane z innych miejsc systemu do danego użytkownika; powinniśmy mieć możliwość oznaczania wiadomości przeczytanych oraz filtrowania ich pod kątem występującego w nich tekstu.
5. Wiadomości tworzą się podczas wszelkich działań, związanych z danym użytkownikiem tj. podczas przypisania użytkownika jako menedżera albo członka projektu (wiadomość typu 1), podczas przypisywania mu zadania (wiadomość typu 2) oraz podczas zmiany stanu zadania do którego jest przypisany (wiadomość typu 3). Wiadomości poszczególnych typów są wyróżniane np. poprzez kolorowanie.

#### Zadanie na 5

Zadanie na 4 oraz:
1. Nowa wiadomość skutkuje alertem o nowej wiadomości, bez względu na to, w którym punkcie nawigacji jesteśmy.
2. Interfejs administratora (rola 0); zakładka która pozwala na tworzenie/modyfikację/usuwanie użytkowników w odpowiednich rolach; modyfikacja obejmuje również zmianę haseł (również tym, którzy zdefiniowani są poza kolekcją users). Zmiany na poziomie użytkownika skutkują alertami do modyfikowanych użytkowników.
3. Menedżerowie i członkowie projektu mogą oglądać jego graficzną reprezentację w postaci diagramu czasowego z zadaniami (najlepiej jako diagram Gantta). Przykładowe wsparcie można uzyskać z https://krispo.github.io/angular-nvd3/ lub https://www.npmjs.com/package/gantt-schedule-timeline-calendar lub https://www.npmjs.com/package/angular-gantt