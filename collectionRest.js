const collectionConfig = require('./collectionConfig')
const db = require('./db')
const sendRefresh = require('./sendRefresh')

module.exports = (collectionNames) => (req, res) => {
    let collectionName = req.params.collection
    let aggr = []
    if(!collectionName) {
        res.status(400).json({ error: 'Invalid URL - no collection given' })
        return
    }
    if(!collectionNames.includes(collectionName)) {
        res.status(400).json({ error: 'Invalid URL - the collection is not allowed for the endpoint' })
        return
    }
    let collection = db.connection.collection(req.params.collection)
    let _id = req.query._id
    if(_id) {
        try {
            _id = db.ObjectId(req.query._id)
        } catch(ex) {
            res.status(400).json({ error: 'Wrong _id format' })
            return
        } 
    }
    switch(req.method) {
        case 'GET':
            let limit = null
            if(req.query.limit) {
                let newLimit = parseInt(req.query.limit)
                if(!isNaN(newLimit) && newLimit > 0) {
                    limit = newLimit
                }
            }
            aggr.length = 0
            if(collectionConfig[collectionName] && collectionConfig[collectionName].getAggregate) {
                aggr = aggr.concat(collectionConfig[collectionName].getAggregate)
            }
            if(req.query.search && collectionConfig[collectionName] && collectionConfig[collectionName].filter) {
                let filter = collectionConfig[collectionName].filter(req.query.search)
                if(filter) aggr.push(filter)
            }
            if(limit) {
                aggr.push({ $limit: limit })
            }
            if(_id) {
                aggr.unshift({ $match: { _id } })
            }
            collection.aggregate(aggr, { collation: { locale : db.collation }}).toArray((err, data) => {
                if(err) {
                    res.status(500).json({ error: err.message })
                } else {
                    res.json(data)
                }       
            })                               
            break
        case 'POST':
            if(collectionConfig[collectionName] && collectionConfig[collectionName].prepareData) {
                let err = collectionConfig[collectionName].prepareData(req.body)
                if(err) {
                    res.status(422).json(err)
                    break
                }
            }
            collection.insertOne(req.body, (err, insertData) => {
                if(err) {
                    res.status(400).json({ error: err.message })
                } else {
                    sendRefresh(req, collectionName)
                    res.json(insertData)
                }
            })
            break
        case 'PUT':
            delete req.body._id
            if(collectionConfig[collectionName] && collectionConfig[collectionName].prepareData) {
                let err = collectionConfig[collectionName].prepareData(req.body)
                if(err) {
                    res.status(422).json(err)
                    break
                }
            }
            collection.findOneAndUpdate({ _id }, { $set: req.body }, { returnDocument: 'after' }, (err, updateData) => {
                if(err) {
                    res.status(400).json({ error: err.message })
                } else {
                    sendRefresh(req, collectionName)
                    res.json(updateData.value)
                }
            })
            break
        case 'DELETE':
            collection.deleteOne({ _id }, (err, deleteData) => {
                if(err) {
                    res.status(400).json({ error: err.message })
                } else {
                    sendRefresh(req, collectionName)
                    res.json(deleteData)
                }
            })
            break
        default:
            res.status(405).json({ error: 'not implemented' })
    }
}