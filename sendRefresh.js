const common = require('./common')

module.exports = (req, collectionName) => {
    console.log('about refreshing signal', collectionName)
    req.sessionStore.all((err, sessions) => {
        if(!err) {
            console.log('prepare refreshing signal', collectionName)
            let refresh = JSON.stringify({ type: 'refresh', collection: collectionName })
            common.wsInstance.getWss().clients.forEach((client) => {
                console.log('detecting ws', client.sessionid)
                if(client.readyState == client.OPEN) {
                    let to = sessions[client.sessionid] && sessions[client.sessionid].passport ? sessions[client.sessionid].passport.user : null
                    if(to) {
                        console.log('WEBSOCKET sending', refresh, 'to', to)
                        client.send(refresh)
                    }
                }
            })   
        }
    })
}