const db = require('./db')

module.exports = (req, res) => {
    let q1 = db.connection.collection('persons').count()
    let q2 = db.connection.collection('projects').count()
    Promise.all([ q1, q2 ]).then((values) => {
        res.json({ persons: values[0], projects: values[1] })
    })
}