const db = require('./db')

const auth = module.exports = {
    checkCredentials: (username, password, nextTick) => {
        db.users.findOne({ username: username, password: password }, (err, user) => {
            return nextTick(null, err || !user ? false : user)
        })
    },
    checkIfInRole: (roleNums) => (req, res, nextTick) => {
        let intersection = []
        if(roleNums == null) {
            intersection.push(-1)
        } else {
            roleNums.forEach((roleNum) => {
                if(req.user && req.user.roles && req.user.roles.includes(roleNum)) {
                    intersection.push(roleNum)
                }
            })
        }
        if(!req.isAuthenticated()) {
            res.status(401).json({ error: 'Not authorized' })
        } else if(intersection.length > 0) {
            return nextTick()
        } else {
            res.status(403).json({ error: 'Permission denied' })
        }
    },
    serialize: (user, nextTick) => {
        nextTick(null, user.username)
    },
    deserialize: (username, nextTick) => {
        db.users.findOne({ username }, (err, user) => {
            if(err || !user) {
                nextTick('No such user', null)
            } else {
                nextTick(null, user)
            }
        })
    },
    login: (req, res) => {
        auth.whoami(req, res)
    },
    logout: (req, res) => {
        req.logout(() => { res.json({}) })
    },
    whoami: (req, res) => {
        req.session.roles = req.user ? req.user.roles : []
        req.session.save()
        let data = {}
        if(req.user) {
            data.username = req.user.username
            data.roles = req.user.roles
            data.sessionid = req.session.id
        }
        res.json(data)
    },
    errorHandler: (err, req, res, nextTick) => {
        res.json({ error: err.message })
    }
}